window.onload = () => {
    let counter = 0, intervalHandler, flag = false;
    const get = id => document.getElementById(id);

    const count = () => {
        counter++;
        get("Seconds").innerHTML = String(counter % 60).padStart(2, "0");
        get("Minutes").innerHTML = String(Math.trunc(counter / 60) % 60).padStart(2, "0");
        get("Hours").innerHTML = String(Math.trunc(counter / 3600) % 100).padStart(2, "0");
    };

    get("startButton").onclick = () => {
        if (!flag) {
            intervalHandler = setInterval(count, 1000);
            flag = true;
        }
        let stopwatchDisplay = document.getElementsByClassName('stopwatch-display')[0];
        stopwatchDisplay.classList.remove("red");
        stopwatchDisplay.classList.add("green");
    };

    get("stopButton").onclick = () => {
        clearInterval(intervalHandler);
        let stopwatchDisplay = document.getElementsByClassName('stopwatch-display')[0];
        stopwatchDisplay.classList.remove("green");
        stopwatchDisplay.classList.add("red");
        flag = false;
    };

    get("ResetButton").onclick = () => {
        clearInterval(intervalHandler);
        flag = false;
        counter = 0;

        get("Seconds").innerHTML = "00";
        get("Minutes").innerHTML = "00";
        get("Hours").innerHTML = "00";
        let stopwatchDisplay = document.getElementsByClassName('stopwatch-display')[0];
        stopwatchDisplay.classList.remove("green");
        stopwatchDisplay.classList.remove("red");
        stopwatchDisplay.classList.add("silver");
    };
}
