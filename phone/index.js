function phone_pattern_test() {
    const pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
    const phoneInput = document.getElementById('phone_input');
    const errorText = document.getElementById('error');

    if (pattern.test(phoneInput.value)) {
        errorText.innerHTML = "";
        document.body.style.backgroundColor = "green";
        setTimeout(() => {
            document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        }, 1000);
    } else {
        errorText.innerHTML = "Неправильний формат номера";
        phoneInput.style.color = 'black';
    }
}